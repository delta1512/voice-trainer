class FFTArray {
  constructor(fftArray) {
    this.a = fftArray
    this.length = fftArray.length
    this.samples = 1
  }

  add(fftArray) {
    if (fftArray.length != this.length) {
      console.log('WARNING: Invalid length array provided to the FFTArray object')
    }

    for (let i = 0; i < fftArray.length; i++) {
      this.a[i] += fftArray[i]
    }
  }

  sub(fftArray) {
    if (fftArray.length != this.length) {
      console.log('WARNING: Invalid length array provided to the FFTArray object')
    }

    for (let i = 0; i < fftArray.length; i++) {
      this.a[i] -= fftArray[i]
    }
  }

  subPos(fftArray) { // Aubtraction with a floor at 0
    if (fftArray.length != this.length) {
      console.log('WARNING: Invalid length array provided to the FFTArray object')
    }

    for (let i = 0; i < fftArray.length; i++) {
      this.a[i] = max(this.a[i] - fftArray[i], 0)
    }
  }

  div(mag) {
    for (let i = 0; i < this.length; i++) {
      this.a[i] /= mag
    }
  }

  // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online
  rollAvg(fftArray) {
    let sample = new FFTArray(fftArray)
    let old = new FFTArray(this.a)
    this.samples += 1

    sample.sub(old.a)
    sample.div(this.samples)
    this.add(sample.a)
  }
}
