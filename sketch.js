var mic
var fft
var trackTimeUser = 0
var trackTimeSound = 0
var userVoiceTrail
var soundVoiceTrail
var theNoise
var sounds = []
var score = 0
// These variables can be changed by external scripts
var mode = 'debug'
var gender = 0

var MIN_HUMAN_VOCAL_FREQ = 50
var MAX_HUMAN_VOCAL_FREQ = 1500

var FFTScoringFunc = calcAvgNBest

var MODES = {
  'prespeak' : preSpeakFlow,
  'speak' : speakFlow,
  'listen' : listenFlow,
  'noise' : denoisingFlow,
  'debug' : showFFTFreq,
  'score' : calcScoreFlow
}

var MODE_DEPS = {
  'prespeak' : [],
  'speak' : [
    function() {userVoiceTrail = new Trail(0, 0, 0, 0, width); trackTimeUser = 0},
    setInputRecordMic,
    function() {setTimeout(function() {changeMode('score')}, sounds[gender].duration()*1000 + 100)}
  ],
  'listen' : [
      function() {soundVoiceTrail = new Trail(0, 0, 0, 0, width); trackTimeSound = 0},
      setInputRecordSketch,
      function() {setTimeout(function() {changeMode('prespeak')}, sounds[gender].duration()*1000)},
      function() {sounds[gender].play()}
    ],
  'noise' : [setInputRecordMic, function() {setTimeout(function() {changeMode()}, 2000)}],
  'debug' : [setInputRecordMic],
  'score' : [doCalcScore]
}

var DEBUG_MODES = ['debug', 'noise', 'listen', 'prespeak', 'speak', 'score']


function preload() {
  sounds.push(loadSound('female.mp3'))
  sounds.push(loadSound('male.mp3'))
}


function setup() {
  frameRate(60)
  createCanvas(windowWidth, windowHeight)
  mic = new p5.AudioIn()
  fft = new p5.FFT()
  setInputRecordMic()
}


function setInputRecordMic() {
  mic.start()
  fft.setInput(mic)
}


function setInputRecordSketch() {
  mic.stop()
  fft = new p5.FFT()
}


// Gets FFT with optional denoising
function getFFTFreq(noiseFFTArray=undefined) {
  let spectrum = new FFTArray(fft.analyze())

  // Zero-out the non-vocal range
  // For 1024 bins, it's approximately 20Hz per bin
  for (let i = MAX_HUMAN_VOCAL_FREQ / 20; i < spectrum.a.length; i++) {
    spectrum.a[i] = 0
  }

  if (noiseFFTArray) {
    spectrum.subPos(noiseFFTArray.a)

    return spectrum.a
  }

  return spectrum.a
}


// A scoring function that splits the array in half, treats the first half as
// positive, the later negative and adds the amplitudes of each together
function calcSumHalf(fftArray) {
  let firstHalf = fftArray.slice(0, fftArray.length / 2)
  let lastHalf = fftArray.slice(fftArray.length / 2 + 1)
  let maxVal = 255 * fftArray.length
  let s = 0

  for (let i of firstHalf) {
    s += i
  }

  for (let i of lastHalf) {
    s -= i
  }

  return map(s, -maxVal, maxVal, -height/2, height/2)
}


// A scoring function that plots the average of the bin indices weigthed on
// the amplitude of each bin
function calcAvgBins(fftArray) {
  let n = 1
  let s = 0

  for (let i = 0; i < fftArray.length; i++) {
    s += i * fftArray[i]
    n += fftArray[i]
  }

  return map(s / n, 0, MAX_HUMAN_VOCAL_FREQ / 20, -height/2, height/2)
}


// A scoring function that is the same as calcAvgBins but us limited to the
// first quarter of the FFT spectrum
function calcAvgBinsQuarter(fftArray) {
  let n = 1
  let s = 0

  for (let i = 0; i < fftArray.length / 4; i++) {
    s += i * fftArray[i]
    n += fftArray[i]
  }

  return map(s / n, 0, fftArray.length / 4, -height/2, height/2)
}


// Function that calculates a discrete set of frequency averages and selects the
// loudest one to be displayed.
function calcEnergyRange(fftArray, start=50, stop=600, split=12) {
  let result = 0
  let resultE = 0

  for (let i = 1; i < split + 1; i++) {
    let prevV = (i - 1) / split
    let newV = i / split
    let currentE = fft.getEnergy(lerp(start, stop, prevV), lerp(start, stop, newV))

    if (resultE < currentE) {
      resultE = currentE
      result = i
    }
  }

  return map(result, 0, split, -height/2, height/2)
}


function calcMaxBin(fftArray) {
  let result = 0
  let resultAmp = 0

  for (let i = 0; i < fftArray.length; i++) {
    let amp =fftArray[i]
    if (amp > resultAmp && amp > 85) {
      result = i
      resultAmp = amp
    }
  }

  return map(result, 0, MAX_HUMAN_VOCAL_FREQ / 20, -height/2, height/2)
}


// Scoring function that selects the loudest harmonic within the range of a
// human voice.
function calcMaxHumanVoiceHarmonic(fftArray) {
  let result = 0
  let resultE = 0

  for (let i = 1; i < MAX_HUMAN_VOCAL_FREQ / MIN_HUMAN_VOCAL_FREQ; i++) {
    let currentE = fft.getEnergy(MIN_HUMAN_VOCAL_FREQ * i)

    if (resultE < currentE && currentE > 85) {
      resultE = currentE
      result = i
    }
  }

  return map(result, 0, MAX_HUMAN_VOCAL_FREQ / MIN_HUMAN_VOCAL_FREQ, -height/2, height/2)
}


function calcAvgNBest(fftArray) {
  // Attach the index to each element in the array by turning them all into objects
  let newArray = []
  let n = 10 // This is how many peaks to look at
  let s = 0

  for (let i = 0; i < fftArray.length; i++) {
    let newObj = new Object()
    newObj.index = i
    newObj.val = fftArray[i]
    newArray.push(newObj)
  }

  newArray.sort(function(a, b) { return b.val - a.val })

  for (let i = 0; i < n; i++) {
    if (newArray[i].val >= 85) {
      s += newArray[i].index
    }
  }

  return map(s / n, 0, MAX_HUMAN_VOCAL_FREQ / 20, -height/2, height/2)
}


function draw() {
  if (getAudioContext().state == 'suspended') {
    background(20);
    fill(255)
    stroke(255)
    strokeWeight(1)
    textAlign(CENTER, CENTER)
    text('Click to start', width/2, height/2)
  } else {
    MODES[mode]()
  }
}


function doCalcScore() {
  let n = soundVoiceTrail.points.length
  score = 0;

  for (let i = 0; i < n; i++) {
    score += (height/2) / (abs(soundVoiceTrail.points[i][1] - userVoiceTrail.points[i][1]) + 1)
  }

  score = score.toFixed(0)
}


function calcScoreFlow() {
  background(20)
  fill(255)
  stroke(255)
  strokeWeight(1)
  textAlign(CENTER, CENTER)
  text('Your score is ' + score, width/2, height/2)
}


function preSpeakFlow() {
  background(20)
  fill(255)
  stroke(255)
  strokeWeight(1)
  textAlign(CENTER, CENTER)
  text('Click when ready to repeat', width/2, height/2)

  translate(0, height/2)

  strokeWeight(5)
  soundVoiceTrail.draw()
}


function speakFlow() {
  background(20)

  translate(0, height/2)

  let spectrum = getFFTFreq(theNoise)
  let c = color(255, 0, 255)

  strokeWeight(5)
  soundVoiceTrail.draw()
  strokeWeight(2)
  userVoiceTrail.draw()

  if (trackTimeUser < trackTimeSound) {
    userVoiceTrail.addPoint(trackTimeUser, FFTScoringFunc(spectrum), c)
    trackTimeUser += 2
  }
}


function listenFlow() {
  background(20)

  translate(0, height/2)

  let spectrum = getFFTFreq()
  let c = color(0, 255, 255)

  strokeWeight(2)
  soundVoiceTrail.draw()
  soundVoiceTrail.addPoint(trackTimeSound, FFTScoringFunc(spectrum), c)

  trackTimeSound += 2
}


function denoisingFlow() {
  background(20)
  fill(255)
  stroke(255)
  strokeWeight(1)
  textAlign(CENTER, CENTER)
  text('Removing noise, don\'t make any sounds...', width/2, height/2)

  let spectrum = getFFTFreq()

  if (!theNoise) {
    theNoise = new FFTArray(spectrum)
  } else {
    theNoise.rollAvg(spectrum)
  }
}


function showFFTFreq() {
  background(20)

  let spectrum = getFFTFreq(theNoise)

  noStroke()
  fill(255, 0, 255)
  for (let i = 0; i < spectrum.length; i++) {
    let x = map(i, 0, spectrum.length, 0, width)
    let h = -height + map(spectrum[i], 0, 255, height, 0)
    rect(x, height, width / spectrum.length, h)
  }
}


function changeMode(nextMode=undefined) {
  if (getAudioContext().state == 'suspended') {
    userStartAudio()
    return
  }

  if (!nextMode) {
    nextMode = DEBUG_MODES[(DEBUG_MODES.indexOf(mode) + 1) % DEBUG_MODES.length]
  }

  for (let dep of MODE_DEPS[nextMode]) {
    dep()
  }

  mode = nextMode
}


function mouseReleased() {
  changeMode()
}
